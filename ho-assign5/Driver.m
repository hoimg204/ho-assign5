//
//  Driver.m
//  ho-assign5
//
//  Created by Henrique de Oliveira Carvalho on 2014-10-08.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 1:
//  Write a Rectangle method called containsPoint: that takes an XYPoint object
//  as its argument:
//  -(BOOL) containsPoint: (XYPoint *) aPoint;
//  Have the method return the BOOL value YES if the rectangle encloses the
//  specified point and NO if it does not.
//
// Inputs:  None
// Outputs: Return a variable BOOL value YES or NO if the rectangle encloses the
//  specified point or not.
//
// ******************************************************************************
//
// Problem Statement 2:
//  Write a Rectangle method called intersect: that takes a rectangle as an
//  argument and returns a rectangle representing the overlapping area between
//  the two rectangles. For example, given the two rectangles shown in Fig. 8.10,
//  the method should return a rectangle whose origin is at (400, 420), whose
//  width is 50, and whose height is 60.
//  If the rectangles do not intersect, return one whose width and height are zero
//  and whose origin is at (0,0).
//
// Inputs:   None
// Outputs:  Return a rectangle representing the overlapping area between
//  the two rectangles
//
// ******************************************************************************
//
// Problem Statement 3:
//  Write a method for the Rectangle class called draw that draws a rectangle
//  using dashes and vertical bar characters. The following code sequence
//  Rectangle *myRect = [[Rectangle alloc] init]; [myRect setWidth: 10 andHeight: 3];
//  [myRect draw];
//  would produce the following output:
//   ----------
//   |        |
//   |        |
//   |        |
//   ----------
//
// Inputs:   none
// Outputs:  Draw a rectangle;
//
// *********************************************************************************

#import "Driver.h"
#import "Rectangle.h"
#import "XYPoint.h"

@implementation Driver
- (void) run
{
  // Question 6
  Rectangle * rect = [[Rectangle alloc] init];
  XYPoint * origin = [[XYPoint alloc] init];
  XYPoint * point = [[XYPoint alloc] init];

  [point setX:3 andY:3];
  [origin setX:1 andY:2];
  [rect setWidth:8 andHeight:4];

  // testing the values
  NSLog(@"Question 6:\n");
  NSLog(@"Point at (%d, %d).", [point x], [point y]);
  NSLog(@"Origin at (%d, %d).", rect.origin.x, rect.origin.y);
  NSLog(@"The height is %d and the width is %d).", rect.height, rect.width);

  if ([rect containsPoint:point] == YES)
  {
    NSLog(@"Point is enclosed.");
  }
  else
  {
    NSLog(@"Point is not enclosed.");
  }

  // Question 7
  Rectangle * Rect1 = [Rectangle new];
  Rectangle * Rect2 = [Rectangle new];
  Rectangle * newRect = [Rectangle new];

  XYPoint * Point1 = [XYPoint new];
  XYPoint * Point2 = [XYPoint new];

  [Rect1 setWidth:250 andHeight:75];
  [Point1 setX:200 andY:420];
  Rect1.origin = Point1;

  [Rect2 setWidth:100 andHeight:180];
  [Point2 setX:400 andY:300];
  Rect2.origin = Point2;

  newRect = [Rect1 intersect:Rect2];

  NSLog(@"Question 7:\n");
  NSLog(@"Origin is at (%d,%d)", newRect.origin.x, newRect.origin.y);
  NSLog(@"Width is %d and Height is %d", newRect.width, newRect.height);

  // Question 8
  NSLog(@"Question 8:\n");
  Rectangle * myRect = [[Rectangle alloc] init];
  [myRect setWidth:10 andHeight:3];
  [myRect draw];
}
@end
