//
//  main.m
//  ho-assign5
//
//  Created by Henrique de Oliveira Carvalho on 2014-10-07.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 1:
//  Write a Rectangle method called containsPoint: that takes an XYPoint object
//  as its argument:
//  -(BOOL) containsPoint: (XYPoint *) aPoint;
//  Have the method return the BOOL value YES if the rectangle encloses the
//  specified point and NO if it does not.
//
// Inputs:  None
// Outputs: Return a variable BOOL value YES or NO if the rectangle encloses the
//  specified point or not.
//
// ******************************************************************************
//
// Problem Statement 2:
//  Write a Rectangle method called intersect: that takes a rectangle as an
//  argument and returns a rectangle representing the overlapping area between
//  the two rectangles. For example, given the two rectangles shown in Fig. 8.10,
//  the method should return a rectangle whose origin is at (400, 420), whose
//  width is 50, and whose height is 60.
//  If the rectangles do not intersect, return one whose width and height are zero
//  and whose origin is at (0,0).
//
// Inputs:   None
// Outputs:  Return a rectangle representing the overlapping area between
//  the two rectangles
//
// ******************************************************************************
//
// Problem Statement 3:
//  Write a method for the Rectangle class called draw that draws a rectangle
//  using dashes and vertical bar characters. The following code sequence
//  Rectangle *myRect = [[Rectangle alloc] init]; [myRect setWidth: 10 andHeight: 3];
//  [myRect draw];
//  would produce the following output:
//   ----------
//   |        |
//   |        |
//   |        |
//   ----------
//
// Inputs:   none
// Outputs:  Draw a rectangle;
//
// *********************************************************************************

// dia

#import <Foundation/Foundation.h>
#import "Driver.h"

int main(int argc, const char * argv[])
{
  @autoreleasepool {

    // Demonstrating all methods of Rectangle class.
    Driver * test = [[Driver alloc] init];
    [test run];

  }
  return 0;
}


