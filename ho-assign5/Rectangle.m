//
//  Rectangle.m
//  ho-assign5
//
//  Created by Henrique de Oliveira Carvalho on 2014-10-07.
//  Copyright (c) 2014 beta. All rights reserved.
//
// Problem Statement 1:
//  Write a Rectangle method called containsPoint: that takes an XYPoint object
//  as its argument:
//  -(BOOL) containsPoint: (XYPoint *) aPoint;
//  Have the method return the BOOL value YES if the rectangle encloses the
//  specified point and NO if it does not.
//
// Inputs:  None
// Outputs: Return a variable BOOL value YES or NO if the rectangle encloses the
//  specified point or not.
//
// ******************************************************************************
//
// Problem Statement 2:
//  Write a Rectangle method called intersect: that takes a rectangle as an
//  argument and returns a rectangle representing the overlapping area between
//  the two rectangles. For example, given the two rectangles shown in Fig. 8.10,
//  the method should return a rectangle whose origin is at (400, 420), whose
//  width is 50, and whose height is 60.
//  If the rectangles do not intersect, return one whose width and height are zero
//  and whose origin is at (0,0).
//
// Inputs:   None
// Outputs:  Return a rectangle representing the overlapping area between
//  the two rectangles
//
// ******************************************************************************
//
// Problem Statement 3:
//  Write a method for the Rectangle class called draw that draws a rectangle
//  using dashes and vertical bar characters. The following code sequence
//  Rectangle *myRect = [[Rectangle alloc] init]; [myRect setWidth: 10 andHeight: 3];
//  [myRect draw];
//  would produce the following output:
//   ----------
//   |        |
//   |        |
//   |        |
//   ----------
//
// Inputs:   none
// Outputs:  Draw a rectangle;
//
// *********************************************************************************

#import "Rectangle.h"
#import "XYPoint.h"

@implementation Rectangle
{
  XYPoint * origin;
}

@synthesize width, height;

- (void) setWidth:(int)w andHeight:(int)h
{
  width = w;
  height = h;
}

- (void) setOrigin:(XYPoint *)pt
{
  origin = pt;
}

- (int) area
{
  return width * height;
}

- (int) perimeter
{
  return (width + height) * 2;
}

- (XYPoint *) origin
{
  return origin;
}

// Question 6
- (BOOL) containsPoint:(XYPoint *)aPoint
{
  int x1 = origin.x;
  int x2 = origin.x + width;
  int y1 = origin.y;
  int y2 = origin.y + height;

  // A point encloses a rectangle when:
  // x <= aPoint.x <= x+h
  // y <= aPoint.y <= y+w
  if ((x1 <= aPoint.x && aPoint.x <= x2) && (y1 <= aPoint.y && aPoint.y <= y2))
  {
    return YES;
  }
  else
  {
    return NO;
  }
}

// Question 7
- (Rectangle *) intersect:(Rectangle *)rect
{
  XYPoint * newOrigin = [XYPoint new];
  Rectangle * newRect = [[Rectangle alloc] init];

  // Setting the values of new rentangle with original rentangle
  int x1 = origin.x;
  int y1 = origin.y;
  int x2 = origin.x + width;
  int y2 = origin.y + height;

  // checking if x1 and y1 of values argument are greater than first rentangle
  if (x1 < rect.origin.x)
  {
    x1 = rect.origin.x;
  }

  if (y1 < rect.origin.y)
  {
    y1 = rect.origin.y;
  }

  // checking if x2 and y2 of values argument are less than first rentangle
  if (x2 > rect.origin.x + rect.width)
  {
    x2 = rect.origin.x + rect.width;
  }

  if (y2 > rect.origin.y + rect.height)
  {
    y2 = rect.origin.y + rect.height;
  }

  // Checking if the rectangles do not intersect
  if (x1 <= x2 && y1 <= y2)
  {
    // Calculeting the width and height
    int newWidth = x2 - x1;
    int newHeight = y2 - y1;

    // Setting the origin of new rectangle
    [newOrigin setX:x1 andY:y1];
    [newRect setOrigin:newOrigin];
    [newRect setWidth:newWidth andHeight:newHeight];
  }

  return newRect;
}

// Question 8
- (void) draw
{
  // Top of Rectangle
  for (int count1 = width; count1 > 0; count1--)
  {
    printf("-");
  }

  // Middle of Rectangle
  for (int count2 = height; count2 > 0; count2--)
  {
    printf("\n|");
    for (int count3 = width; count3 > 0; count3--)
    {
      printf(" ");
    }
    printf("|\n");
  }

  // Bottom of Rectangle
  for (int count4 = width; count4 > 0; count4--)
  {
    printf("-");
  }

  printf("\n");
}

@end
